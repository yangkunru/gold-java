package com.tw.questionEasy;

public class HtmlEscaper {
    // TODO:
    //   You can add additional members or blocks of code here if you want.
    // <-start-
    
    // --end-->

    /**
     * This function will try escaping characters according to the rules defined in HTML 4.01
     * The rules are as follows:
     *
     * (1) Every `"` character will be escaped to `&quot;`
     * (2) Every `'` character will be escaped to `&#39;`
     * (3) Every `&` character will be escaped to `&amp;`
     * (4) Every `<` character will be escaped to `&lt;`
     * (5) Every `>` character will be escaped to `&gt;`
     *
     * @param text The text to escape.
     * @return The escaped string.
     */
    public static String escape(String text) {
        // TODO:
        //   Please implement the method
        // <-start-
        if(text == null) {
            throw new IllegalArgumentException();
        }
        StringBuffer escapedText = new StringBuffer("");
        for (int i = 0; i < text.length(); i++) {
            switch (text.charAt(i)) {
                case '"':
                    escapedText.append("&quot;");
                    break;
                case '\'':
                    escapedText.append("&#39;");
                    break;
                case '&':
                    escapedText.append("&amp;");
                    break;
                case '<':
                    escapedText.append("&lt;");
                    break;
                case '>':
                    escapedText.append("&gt;");
                    break;
                default:
                    escapedText.append(text.charAt(i));
            }
        }
        return escapedText.toString();
        // --end-->
    }
}
