package com.tw.questionMedium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book... books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     * should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String... tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (tags.length == 0) {
            return new ArrayList<Book>();
        }
        List<Book> bookList = new ArrayList<Book>();
        for (int i = 0; i < tags.length; i++) {
            for (int j = 0; j < this.books.size(); j++) {
                if (this.books.get(j).getTags().contains(tags[i])) {
                    bookList.add(this.books.get(j));
                }
            }
        }
        return booksOrder(bookList);
        // --end-->
    }

    // TODO:
    //   You can add additional methods here if you want.
    // <-start-
    private List<Book> booksOrder(List<Book> bookList) {
        List<Book> orderedBooksList = new ArrayList<Book>();
        HashMap<Long, Book> bookHashMap = new HashMap<Long, Book>();
        for (int i = 0; i < bookList.size(); i++) {
            bookHashMap.put(Long.parseLong(bookList.get(i).getIsbn()), bookList.get(i));
        }
        Object[] keys = bookHashMap.keySet().toArray();
        Arrays.sort(keys);
        for (int i = 0; i < keys.length; i++) {
            orderedBooksList.add(bookHashMap.get(keys[i]));
        }
        return orderedBooksList;
    }
    // --end-->
}
